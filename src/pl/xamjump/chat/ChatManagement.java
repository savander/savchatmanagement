/**
 *pl.xamjump.chat
 *ChatManagement
 *ChatManagement.java
 *ChatManagement
 *30-11-2013
 *$
 */
package pl.xamjump.chat;

import java.util.HashMap;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import org.bukkit.plugin.java.JavaPlugin;

/**
 * @author Savander
 *
 */
public class ChatManagement extends JavaPlugin implements Listener {
	

	protected static final Logger Log =  Logger.getLogger("Minecraft");
	public HashMap<String, Boolean> chat = new HashMap<String, Boolean>();
	
	
	
	
	
	@Override
	public void onEnable() {
		Log.info(this.getName()+ " Plugin wlaczony");
		getServer().getPluginManager().registerEvents(this, this);
		
		chat.put("enable", true);
		chat.put("chat", true);
	}

	@Override
	public void onDisable() {
		Log.info(this.getName()+ " Plugin wylaczony");
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		Player p = (Player) sender;
		if(sender instanceof Player){
			if(label.equalsIgnoreCase("pkchat") && p.hasPermission("chat.manage")){
				
				if(args[0].equalsIgnoreCase("off")){
					chat.put("chat", false);
					
					if(chat.get("enable") == true){
						
						for(int i=0; i<100;i++){
							Bukkit.broadcastMessage("");
							
							if(i==90){
								Bukkit.broadcastMessage("�c<<--------Chat wylaczony!-------->>");
							}
						}
					}
					else{
						p.sendMessage("Chat jest juz wylaczony");
					}
						chat.put("enable", false);
					}
				
				else if(args[0].equalsIgnoreCase("on")){
					chat.put("chat", true);
					
					if(chat.get("enable") == false){
						Bukkit.broadcastMessage("�aChat wlaczony!");
					}
					else{
						p.sendMessage("Chat jest juz wlaczony");
					}
					
					chat.put("enable", true);
				}
					
			}else{
					p.sendMessage("�cYou don't have permissions");
					
				}
		}
		else{
			CommandSender cs = (CommandSender) sender;
			cs.sendMessage("Nie mozna uzyc z konsoli");
		}
		
		
		return false;
	}
	
	@EventHandler
	public void chatManage(AsyncPlayerChatEvent evt){
		Player p = evt.getPlayer();
		if(chat.get("chat") == false){
			if(p.hasPermission("chat.canchat") || p.hasPermission("chat.manage")) {
				evt.setCancelled(false);
			}
			else{
				p.sendMessage("Chat zostal wylaczony, nie mozesz pisac");
				evt.setCancelled(true);
			}
		}
		else{
			evt.setCancelled(false);
		}
	}
}
